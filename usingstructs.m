clear;
clc;
close all;

A = imread('small.png');
figure,imshow(A);
sum_pixels = numel(A);
[rows,cols] = size(A);

%find min and max pixel values
min = min(A(:))
max = max(A(:))

range = max - min + 1;

%create frequency array,which contains the amount of times each pixel value exists in
%array A
frequency = zeros(range, 2); 
for i = 1:range
    frequency(i, 1) = min + i - 1;
end

for i = 1:rows
    for j = 1:cols
        for k = 1:max+1
            if A(i,j) == frequency(k,1)
                frequency(k,2) = frequency(k,2) + 1;
                break;
            end
        end
    end
end
%create probabilities array
prob = zeros(range, 2);

for i=1:range
    prob(i,1) = min + i - 1;
    prob(i,2) = frequency(i,2) / sum_pixels;
end
%check value of probabilities array
S = sum(prob(:,2));
assert(abs(S - 1) < 1e-5);

%remove rows with probability=0
prob = prob(prob(:,2) > 0, :);
frequency = frequency(frequency(:,2) > 0, :);

prob = sortrows(prob, 2, 'descend');
dict_rows = size(prob, 1);

nodes(dict_rows) = struct('pixel_value', 0, 'probability', 0, 'left', 0, 'right', 0);
for i=1:dict_rows
    nodes(i) = struct('pixel_value', prob(i, 1), 'probability', prob(i,2), 'left', 0, 'right', 0);
end

T = struct2table(nodes); % convert the struct array to a table
sortedT = sortrows(T, 'probability', 'descend'); % sort the table by 'probability'
nodes = table2struct(sortedT); % change it back to struct array if necessary

while size(nodes, 1) > 1
    nodes(end-1) = struct('pixel_value', [nodes(end).pixel_value, nodes(end-1).pixel_value], 'probability', nodes(end).probability + nodes(end-1).probability, 'left', nodes(end), 'right', nodes(end-1));
    nodes(end) = [];


    i = length(nodes);
     while i >= 2 && nodes(i - 1).probability < nodes(i).probability
         [nodes(i), nodes(i-1)] = swap(nodes(i), nodes(i-1));
         i = i - 1;
     end
end

dict = getDict(nodes(1), [], {});
huffman_word = cell(1, rows*cols);
count = 1;

for i=1:rows    
    for j=1:cols
        for k=1:dict_rows
            if A(i,j) == dict{k, 1}(1)
                huffman_word(1, count) = dict(k,2);
                count = count+1;
                break;
            end
        end
    end
    
end

huffman_word = mat2str(cell2mat(huffman_word));

word_length = dict;
num_bits = 0;
for i = 1:dict_rows
    for j = 1:dict_rows
        if word_length{i, 1} == frequency(j, 1)
            word_length{i, 3} = frequency(j, 2);
            break;
        end
    end
    
    word_length{i, 4} = length(word_length{i, 2});
    word_length{i, 5} = word_length{i, 3} * word_length{i, 4};
    num_bits = num_bits + word_length{i, 5};
end

init_num_bits = rows * cols * 8;
fprintf('%d - %d = %d bits saved\n', init_num_bits, num_bits, init_num_bits - num_bits);

%find avglen --the average length among all codewords in the dictionary,
%weighted according to the probabilities in the vector prob

for i=1:length(word_length)
    word_length{i,6}=(word_length{i,3}/sum_pixels);
    word_length{i,7}=(word_length{i,3}/sum_pixels)*word_length{i,4};

end
Z=sortrows(word_length,[4 6],'descend');
disp(huffman_word)        
avglen=sum([word_length{:,7}])
figure;scatter(cell2mat(Z(:,7)),cell2mat(Z(:,4)))